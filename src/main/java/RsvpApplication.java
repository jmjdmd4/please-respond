import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 *
 * Send HTTP request from Java to Meetup.com RSVP HTTP data stream
 * Runtime and package using Java:Maven
 *
 * author: Jacquelyne L. Wilson
 */
public class RsvpApplication {
    private static final String MEETUP_RSVPS_URL = "https://stream.meetup.com/2/rsvps";
    private static final int TIMEOUT_DEFAULT = 60000;

    private static int totalRSVPRead = 0;

    /**
     * Read the Meetup.com in an input stream
     * @throws IOException exception for no input
     * @return List of events read
     */
    private static List<Event> readInputStreamWithTimeout() throws IOException {
        Event event;
        List<Event> eventList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        // Set the rsvpUrl for the Meetup.com data stream
        URL rsvpUrl = new URL(MEETUP_RSVPS_URL);
        System.out.println("Meetup.com Url: " + rsvpUrl);

        try {
            URLConnection con = rsvpUrl.openConnection();
            //con.setConnectTimeout(TIMEOUT_DEFAULT);  // connection and read timeout always ran longer than 60 secs
            //con.setReadTimeout(TIMEOUT_DEFAULT);
            con.connect();

            if (con.getInputStream() != null) {
                // Create input stream and buffer for reading
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String inputStr;
                long start = System.currentTimeMillis();
                long end = start + TIMEOUT_DEFAULT;
                while ((inputStr = in.readLine()) != null && System.currentTimeMillis() < end) {
                    // total RSVPs that were read
                    totalRSVPRead++;

                    event = objectMapper.readValue(inputStr, Event.class);
                    eventList.addAll(Collections.singleton(event));
                }
            }
        } catch (SocketTimeoutException e) {
            System.out.println("More than " + TIMEOUT_DEFAULT + " elapsed.");
            e.printStackTrace();
        }
        return eventList;
    }

    private static String convertTime(Long eventTime) {
        Date date = new Date(eventTime);
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdformat.format(date);
    }

    private static Map<String, Long> getTop3(List<Event> events) {
        // stream the events and compare by the event name get the top
        //    three rsvps by country.
        // In testing, the Meetup.com stream seem to not have more than one or two rsvps per event.
        // I do not consistently get three top rsvps
        List<Event> collect = events.stream()
                .sorted(Comparator.comparing(Event::getEventName, Comparator.reverseOrder()))
                .limit(3)
                .collect(Collectors.toList());

        Map<String, Long> top3EventCnt = collect.stream()
                .collect(Collectors.groupingBy(Event::getGroupCountry, Collectors.counting()));

        return top3EventCnt;
    }

    /**
     * The following aggregate information should be calculated:
     *      * Total # of RSVPs received
     *      * Date of Event furthest into the future
     *      * URL for the Event furthest into the future
     *      * The top 3 number of RSVPs received per Event host-country
     *
     * @param args
     * @throws IOException
     * @throws RuntimeException
     */
    public static void main(String[] args) throws IOException, RuntimeException {
        // Read the Meet.com rsvp stream and create the aggregations
        List<Event> eventObj = readInputStreamWithTimeout();

        // Date and Url of Event furthest in the future
        Long maxTime = eventObj.stream()
                .map(e -> e.getEventTime())
                .max(Long::compareTo)
                .orElseThrow(() -> new IllegalArgumentException("Expected 'list' to be of size: >= 2."));

        // get the event object of the max time
        Event eventMax = eventObj.stream().filter(val -> val.getEventTime().equals(maxTime)).findFirst().orElse(null);

        System.out.println("Printing Aggregations");
        String eventOutput = totalRSVPRead + "," + convertTime(maxTime) + "," + eventMax.getEventUrl();

        // get the top 3 event counts by county
        Map<String, Long> top3ByCountry = getTop3(eventObj);
        String top3ByCountryStr = top3ByCountry.toString();

        top3ByCountryStr = top3ByCountryStr.substring(1, top3ByCountryStr.length() -1);

        System.out.println(eventOutput + "," + top3ByCountryStr);
    }
}