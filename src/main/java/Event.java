import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Event {
    private Long eventTime;

    private String eventId;
    private String eventName;
    private String eventUrl;
    private String groupCountry;
    private String response;

    private int guests;

    @JsonProperty("event")
    public void setEventMetaData(Map<String, String> event) {
        this.eventId = event.get("event_id");
        this.eventTime = Long.parseLong(event.get("time"));
        this.eventName = event.get("event_name");
        this.eventUrl = event.get("event_url");
    }

    @JsonProperty("group")
    public void setGroupMetaData(Map<String, Object> group) {
        this.groupCountry = group.get("group_country").toString();
    }

//    @JsonProperty("venue")
//    public void setVenueMetaData(Map<String, String> venue) {
//        this.response = venue.get("response");
//        this.guests = parseInt(venue.get("guests"));
//    }


    // Getters and Setters
    public Long getEventTime() {
        return eventTime;
    }

    public String getEventId() {
        return eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventUrl() {
        return eventUrl;
    }

    public void setEventUrl(String eventUrl) {
        this.eventUrl = eventUrl;
    }

    public String getGroupCountry() {
        return groupCountry;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getGuests() {
        return guests;
    }

    public void setGuests(int guests) {
        this.guests = guests;
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventId=" + eventId +
                ", eventTime=" + eventTime +
                ", eventName='" + eventName + '\'' +
                ", eventUrl='" + eventUrl + '\'' +
                ", groupCountry='" + groupCountry + '\'' +
                ", response='" + response + '\'' +
                ", guests=" + guests +
                '}';
    }
}